@extends('layout.app')

@section('content')

	<!-- Content area -->
	<div class="content">
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h4 class="panel-title">Data List User</h4>
				<div class="heading-elements">
			    	<div class="heading-btn-group">							
						<a href="{{ url('user/create') }}" class="btn bg-teal pull-right"><span>Tambah Data</span></a>
					</div>
				</div>
			</div>
			<div class="panel-body padding-panel">
				<div class="table-responsive">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>Name</th>
								<th>Alamat</th>
								<th>No. Telepon</th>
								<th>Email</th>
								<th>Password</th>
								<th>Status</th>
								<th>Aksis</th>
							</tr>
						</thead>
							<tbody>
								@foreach($data as $key => $_data)
									<tr>
										<td>{{ $_data->nama }}</td>
										<td>{{ $_data->alamat }}</td>
										<td>{{ $_data->tlp }}</td>
										<td>{{ $_data->email }}</td>
										<td>{{ $_data->password }}</td>
										<td>{{ $_data->status }}</td>
										<td>
											<a href="" class="btn btn-info btn-xs btn-left"><i class="fa fa-pensil"></i>Edit</a>
                            				<a href="" class="btn btn-danger btn-xs btn-right"><i class="fa fa-trash-o"></i>Delete</a>
										</td>
									</tr>
								@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection