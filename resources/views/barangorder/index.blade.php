@extends('layout.app')

@section('content')

	<!-- Content area -->
	<div class="content">
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h4 class="panel-title">Data List User</h4>
				<div class="heading-elements">
			    	<div class="heading-btn-group">							
						<a href="{{ url('user/create') }}" class="btn bg-teal pull-right"><span>Tambah Data</span></a>
					</div>
				</div>
			</div>
			<div class="panel-body padding-panel">
				<div class="table-responsive">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>Name</th>
								<th>Alamat</th>
								<th>No. Telepon</th>
								<th>Email</th>
								<th>Password</th>
								<th>Status</th>
								<th>Aksis</th>
								<th>Aksis</th>
								<th>Aksis</th>
								<th>Aksis</th>
								<th>Aksis</th>
								<th>Aksis</th>
								<th>Aksis</th>
								<th>Aksis</th>
								<th>Aksis</th>
								<th>Aksis</th>
								<th>Aksis</th>
								<th>Aksis</th>
								<th>Aksis</th>
								<th>Aksis</th>
								<th>Aksis</th>
								<th>Aksis</th>
								<th>Aksis</th>
							</tr>
						</thead>
							<tbody>
								@foreach($barangorder as $key => $_barangorder)
									<tr>
										<td>{{ $_data->id_invoice }}</td>
										<td>{{ $_data->nama_toko_online }}</td>
										<td>{{ $_data->jasa_kiriman }}</td>
										<td>{{ $_data->nama_user }}</td>
										<td>{{ $_data->barang_kirim }}</td>
										<td>{{ $_data->nama_penerima }}</td>
										<td>{{ $_data->tlp_penerima }}</td>
										<td>{{ $_data->alamat }}</td>
										<td>{{ $_data->kecamatan }}</td>
										<td>{{ $_data->kota }}</td>
										<td>{{ $_data->provinsi }}</td>
										<td>{{ $_data->kode_pos }}</td>
										<td>{{ $_data->catatan_khususu }}</td>
										<td>{{ $_data->nama_dropship }}</td>
										<td>{{ $_data->tlp_dropship }}</td>
										<td>{{ $_data->alamat_dropship }}</td>
										<td>{{ $_data->tgl_kirim }}</td>
										<td>{{ $_data->jumlah_barang_kirim }}</td>
										<td>{{ $_data->harga_total_barang }}</td>
										<td>{{ $_data->berat_kiriman }}</td>
										<td>{{ $_data->ongkir_barang }}</td>
										<td>{{ $_data->ongkir_total_barang }}</td>
										<td>{{ $_data->tanggal_barang_diterima }}</td>
										<td>{{ $_data->status_kiriman }}</td>
										<td>
											<a href="" class="btn btn-info btn-xs btn-left"><i class="fa fa-pensil"></i>Edit</a>
                            				<a href="" class="btn btn-danger btn-xs btn-right"><i class="fa fa-trash-o"></i>Delete</a>
										</td>
									</tr>
								@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection