@extends('layout.app')

@section('content')
	<!-- Content area -->
	<div class="page-default">

		<!-- Form horizontal -->
		<div class="panel panel-flat">
			<div class="panel-body">
				{{ Html::ul($errors->all()) }}

				<form data-parsley-validate action="{{url('user')}}" method="POST" class="form-horizontal form-label-left">
					<fieldset class="content-group">
						<legend class="text-bold">Tambah User</legend>

						<div class="form-group">
	                        <label class="control-label col-lg-2">Username<span class="required">*</span>
	                        </label>
	                        <div class="col-md-10 col-sm-10 col-xs-12">
	                          <input type="text" id="username" name="username" required="required" class="form-control col-md-7 col-xs-12">
	                        </div>
                      	</div>

                      	<div class="form-group">
	                        <label class="control-label col-lg-2">Alamat<span class="required">*</span>
	                        </label>
	                        <div class="col-md-10 col-sm-10 col-xs-12">
	                          <input type="text" id="alamat" name="alamat" required="required" class="form-control col-md-7 col-xs-12">
	                        </div>
                      	</div>

                      	<div class="form-group">
	                        <label class="control-label col-lg-2">Telepon<span class="required">*</span>
	                        </label>
	                        <div class="col-md-10 col-sm-10 col-xs-12">
	                          <input type="text" id="telepon" name="telepon" required="required" class="form-control col-md-7 col-xs-12">
	                        </div>
                      	</div>
                       
                      	<div class="form-group">
                        	<label for="email" class="control-label col-lg-2">E-mail<span class="required">*</span></label>
                        	<div class="col-md-10 col-sm-10 col-xs-12">
                         		 <input id="email" name="email" class="form-control col-md-7 col-xs-12" required="required" type="text">
                        	</div>
                     	</div>

                      	<div class="form-group">
                        	<label for="email" class="control-label col-lg-2">Password<span class="required">*</span></label></label>
                        	<div class="col-md-10 col-sm-10 col-xs-12">
                          		<input id="password" name="password" class="form-control col-md-7 col-xs-12" required="required" type="password">
                        	</div>
                      	</div>

						<div class="form-group">
			                <label class="control-label col-lg-2">Status</label>
			                <div class="col-lg-10">
				                <select name="status" class="form-control">
				                    <option value="1">Admin</option>
				                    <option value="0">User</option>
				                </select>
			                </div>
			            </div>
			            <div class="form-group">
                        	<div class="col-md-12 col-xs-12">
                          		<button type="submit" class="btn btn-success pull-right">Submit </button>
                        	</div>
                      	</div>

					</fieldset>
				</form>
			</div>	
		</div>	
	</div>	
@endsection