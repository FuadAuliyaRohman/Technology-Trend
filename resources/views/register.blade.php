<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title> Register </title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="{{ asset('AdminLimitless/assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('AdminLimitless/assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('AdminLimitless/assets/css/core.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('AdminLimitless/assets/css/components.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('AdminLimitless/assets/css/colors.css') }}" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="{{ asset('AdminLimitless/assets/js/plugins/loaders/pace.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('AdminLimitless/assets/js/core/libraries/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('AdminLimitless/assets/js/core/libraries/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('AdminLimitless/assets/js/plugins/loaders/blockui.min.js') }}"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="{{ asset('AdminLimitless/assets/js/plugins/forms/styling/uniform.min.js') }}"></script>

	<script type="text/javascript" src="{{ asset('AdminLimitless/assets/js/core/app.js') }}"></script>
	<script type="text/javascript" src="{{ asset('AdminLimitless/assets/js/pages/login.js') }}"></script>
	<!-- /theme JS files -->

</head>

<body class="login-container">

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">

					<!-- Advanced login -->
					{{ Form::open(array('url' => 'register')) }}
						<div class="panel panel-body login-form">
							<div class="text-center">
								<div class="icon-object border-success text-success"><i class="icon-plus3"></i></div>
								<h5 class="content-group">Create account <small class="display-block">All fields are required</small></h5>
							</div>

							<div class="content-divider text-muted form-group"><span>Your credentials</span></div>

							<div class="form-group has-feedback has-feedback-left">
								<input type="text" class="form-control" name="username" placeholder="Username" required="">
								<div class="form-control-feedback">
									<i class="icon-user text-muted"></i>
								</div>
							</div>

							<div class="form-group has-feedback has-feedback-left">
								<input type="text" class="form-control" name="alamat" placeholder="Alamat" required="">
								<div class="form-control-feedback">
									<i class="icon-home text-muted"></i>
								</div>
							</div>

							<div class="form-group has-feedback has-feedback-left">
								<input type="text" class="form-control" name="tlp" placeholder="Telephone" required="">
								<div class="form-control-feedback">
									<i class="icon-phone text-muted"></i>
								</div>
							</div>

							<div class="form-group has-feedback has-feedback-left">
								<input type="text" class="form-control" name="email" placeholder="Email" required="">
								<div class="form-control-feedback">
									<i class="icon-envelope text-muted"></i>
								</div>
							</div>

							<div class="form-group has-feedback has-feedback-left">
								<input type="text" class="form-control" name="password" placeholder="Password" required="">
								<div class="form-control-feedback">
									<i class="icon-lock text-muted"></i>
								</div>
							</div>
							<input type="hidden" name="status" value="user">
							<input type="hidden" name="status_user" value="0">
							<button type="submit" class="btn bg-teal btn-block btn-lg">Register <i class="icon-circle-right2 position-right"></i></button>
						</div>
					{{ Form::close() }}
					<!-- /advanced login -->


					<!-- Footer -->
					<div class="footer text-center">
						&copy; 2017 <a href="#">PPKK</a> by <a>Team 5 </a>
					</div>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
