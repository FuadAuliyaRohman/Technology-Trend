<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarangOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barang_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_invoice');
            $table->string('nama_toko_online');
            $table->text('jasa_kiriman');
            $table->string('nama_user');
            $table->string('barang_kirim');
            $table->string('nama_penerima');
            $table->string('tlp_penerima');
            $table->text('alamat');
            $table->text('kecamatan');
            $table->text('kota');
            $table->text('provinsi');
            $table->string('kode_pos');
            $table->text('catatan_khusus');
            $table->string('dropship');
            $table->string('nama_dropship');
            $table->string('tlp_dropship');
            $table->text('alamat_dropship');
            $table->date('tanggal_kirim');
            $table->string('jumlah_barang_dikirim');
            $table->string('harga_total_barang');
            $table->string('berat_kiriman');
            $table->string('ongkir_barang');
            $table->string('ongkir_total_barang');
            $table->date('tanggal_barang_diterima');
            $table->string('status_kiriman');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barang_orders');
    }
}
