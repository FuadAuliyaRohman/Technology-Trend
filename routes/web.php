<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('index');
});*/

Route::get('/', 'PageController@index');
Route::get('/login', 'LoginController@index');
Route::post('/login', 'LoginController@auth');//Srcipt untuk pengecekan login
Route::get('/lagout', 'LoginController@lagout');
Route::get('/register', 'RegisterController@index');
Route::post('/register', 'RegisterController@register');

Route::group([ 'prefix' => '/', 'middleware' => 'authcustom' ], function () { 

	Route::get('/dashboard', 'PageController@index');
	
	Route::resource('/user', 'UserController');
	
	Route::get('/barangorder', 'BarangOrderController@index');

});
