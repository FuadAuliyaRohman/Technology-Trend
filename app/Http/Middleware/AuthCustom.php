<?php

namespace App\Http\Middleware;

use Closure,Session,Redirect;

class AuthCustom
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Session::has('auth_name')){
            return $next($request);
        }else{
            return Redirect::to('/login');
        }
    }
}
