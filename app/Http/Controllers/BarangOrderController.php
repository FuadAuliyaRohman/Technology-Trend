<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input, Redirect, Session;
use App\BarangOrder;

class BarangOrderController extends Controller
{
    //
    public function index()
    {
    	$barangorder = BarangOrder::all();
        return view('barangorder.index', compact('barangorder'));
    }
}
