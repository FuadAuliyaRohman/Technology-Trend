<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input, Redirect, Session;
use App\Users;

class LoginController extends Controller
{
    //
    public function index()
    {
    	return view('login');
    }

    public function auth()
    {
        //laravel.com/docs/5.4/html
        $email = Input::get('email');
        $password = Input::get('password');

        $c = Users::where('email', $email)
                ->where('password',$password)
                ->get();

        $d = Users::where('email', $email)
                ->where('password',$password)
                ->where('status_users',"1")
                ->get();

        if (count($c) == 0){ // gagal
            Session::flash('msg', 'Email / password yang anda masukkan salah.' );
            return Redirect::to('login');
        }elseif(count($d) == 0){
            Session::flash('msg', 'Akun anda belum aktif, silahkan hubungi admin untuk mengaktifkan' );
            return Redirect::to('login');
        } else { //sukses
            Session::put('auth_name', $email);
            Session::put('auth_level', $c[0]->status);
            return Redirect::to('/');
        }
    }
    public function lagout(){
        Session::forget('auth_name');
        return Redirect::to('login');    
    }
}
