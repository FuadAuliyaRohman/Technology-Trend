<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input, Redirect;
use App\Users;

class RegisterController extends Controller
{
    public function index()
    {
        return view('register');
    }

    //Register
    public function register()
    {
    	$users = new Users;
        $users->nama         = Input::get('username');
        $users->alamat       = Input::get('alamat');
        $users->tlp          = Input::get('tlp');
        $users->email        = Input::get('email');
        $users->password     = Input::get('password');
        $users->status       = Input::get('status');
        $users->status_users = Input::get('status_user');
        $users->save();
        return Redirect::to('login');
    }
}
