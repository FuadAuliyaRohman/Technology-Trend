<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input, Redirect, Session;
use App\Users;

class UserController extends Controller
{
    //
    public function index()
    {
        $data = Users::all();
        return view('user.index', array('data' => $data));
    }

    public function create()
    {
        return view('user.create');
    }
}
